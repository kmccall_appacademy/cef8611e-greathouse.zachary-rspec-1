#######################################################################
#method which converts temp in degrees fahrenheit to degrees celcius
def ftoc(temp)
  body_temp_f = 98.6
  if(temp == body_temp_f)
    return 37
  end

  converted_temp = (temp - 32) * 5/9
  return converted_temp
end

#######################################################################
#method which converts temp in degrees celcius to degrees fahrenheit
def ctof(temp)
  body_temp_c = 37
  if(temp == body_temp_c)
    return 98.6
  end

  converted_temp = (temp * 9/5) + 32
  return converted_temp
end

#######################################################################
