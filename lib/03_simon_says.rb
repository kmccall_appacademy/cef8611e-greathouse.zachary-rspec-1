#######################################################################
#method which takes a string and returns the same string
def echo(string)
  string
end

#######################################################################
#method which takes a string and returns the it with all capital letters
def shout(string)
  string.upcase
end

#######################################################################
#method which takes a string and repeats it a (number) of times
def repeat(word, number = 2)
  ([word] * number).join(" ")
end

#######################################################################
=begin
method which takes a string and a number and returns the first (number)
of characters in a string.
=end
def start_of_word(string, num)
  sliced_string = string.slice(0, num)
  sliced_string
end

#######################################################################
#method which takes a sentence and returns the first word of a sentence
def first_word(sentence)
  word_array = sentence.split(" ")
  word_array[0]
end

#######################################################################
=begin
method which takes a string and capitalizes all the words except
for "little words".
=end
def titleize(title)
  little_words = ["to", "the", "and", "over", "of"]

  title_words = title.split(" ")

  title_words.each_with_index do |word, i|
    if (i == 0) && (little_words.include? word)
      word.capitalize!
    elsif (i != 0) && (little_words.include? word)
      word.downcase!
    else
      word.capitalize!
    end
  end

  title_words.join(" ")
end

#######################################################################
