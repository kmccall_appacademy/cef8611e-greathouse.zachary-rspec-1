#######################################################################
#method which determines if a character is a vowel
def is_vowel(char)
  vowels = ['a','e','i','o','u']
  char.downcase!
  vowels.include? char
end

#method which translates a word that starts with a vowel to pig latin
def translate_vowel_word(word)
  if is_vowel(word[0])
    return "#{word}ay"
  end
end

#method which translates a word that starts with a consonant to pig latin
def translate_consonant_word(word)
  chars = word.split("")

  if !is_vowel(word[0])

    chars.each_with_index do |char, idx|

      if is_vowel(char) && chars[idx -1] != ('q' || 'Q')
        return word.slice(idx..-1) + word.slice(0...idx) + "ay"
      end

    end
    return "#{word}ay"
  end
end

#method which translates a a string of words to pig latin
def translate(string)
  words = string.split(" ")
  pig_words = []

  words.each do |word|
    chars = word.split("")

    if is_vowel(word[0])
      pig_words.push translate_vowel_word(word)
    else
      pig_words.push translate_consonant_word(word)
    end
  end

  pig_words.join(" ")
end

#######################################################################
