#######################################################################
#method which adds two numbers
def add(num1, num2)
  return num1 + num2
end

#######################################################################
#method which subtracts one number(num1) from another number (num2)
def subtract(num1, num2)
  return num1 - num2
end

#######################################################################
#method which returns the sum of all the numbers in an array
def sum(array)
  total_sum = 0

  array.each do |num|
    total_sum += num
  end

  return total_sum
end

#######################################################################
#method which returns the product of all the numbers in an array
def multiply(array)
  product = 1

  array.each do |num|
    product *= num
  end

  return product
end

#######################################################################
#method which returns a base to the power of an exponent
def power(base, exponent)
  base ** exponent
end

#######################################################################
#method which returns the factorial of a number
def factorial(num)
  product = 1

  if num == 0
    return 1
  end

  i = 1
  while i <= num
    product *= i
    i += 1
  end

  return product
end

#######################################################################ß
